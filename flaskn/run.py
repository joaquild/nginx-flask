from flask import Flask
import os
app = Flask(__name__)

if __name__ == "__main__":
    app.run()

@app.route('/')
def index():
    app_name = os.getenv("APP_NAME")
        
    if app_name:
        return f"Hello from {app_name} running in a Docker container behind Nginx!"

    return "Hello from Flask"
